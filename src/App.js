import logo from './logo.svg';
import './App.css';
import { CounterComponent } from './pages/CounterComponent';
import { useSelector } from 'react-redux';
import { ArticleComponent } from './pages/ArticleComponent';

function App() {
  
  return (
    <div className="App">
      {/* <CounterComponent/> */}
      <ArticleComponent/>
    </div>
  );
}

export default App;
