import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchAllArticles } from "../redux/reducers/articleReducer";
import { fetch_all_articles } from "../services/articles.services";

export const ArticleComponent = () => {
  const articles = useSelector((state) => state.articles.articles);
  const dispatch = useDispatch();
  useEffect(() => {
    fetch_all_articles().then((r) => {
      dispatch(fetchAllArticles(r));
    });
  }, []);
  console.log("articles", articles);
  return (
    <div>
      <ul>
        {articles.map((item, index) => (
          <li key={index}>{item.title}</li>
        ))}
      </ul>
    </div>
  );
};
