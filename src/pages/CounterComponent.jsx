import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { decrement, increment, incrementByAmount } from "../redux/reducers/counterReducer";

export const CounterComponent = () => {
    const counter = useSelector((state) => state.counter.value)
    const dispatch = useDispatch()
    console.log(counter)
    
  return (
    <div>
      <h1>{counter}</h1>
      <button onClick={() => dispatch(increment())}>increase</button>
      <button onClick={() => dispatch(incrementByAmount())}>incease by amount</button>
      <button onClick={() => dispatch(decrement())}>decrease</button>
    </div>
  );
};
