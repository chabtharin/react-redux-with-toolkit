
export const DECREASE = "DECREASE"
export const INCREASE = "INCREASE"

export const increase = (n) => {
    return {
        type: INCREASE,
        num: n
    }
}
export const decrease = (n) => {
    return {
        type: DECREASE
    }
}