import { createSlice } from "@reduxjs/toolkit";
import { fetch_all_articles } from "../../services/articles.services";

export const articleSlice = createSlice({
    name: 'articles',
    initialState: {
        articles: [{name: "test"}]
    },
    reducers: {
        fetchAllArticles: (state, {payload}) => {
            state.articles = payload
        },
        postArticle: (state, {payload}) => {

        }
    }
})

export const {fetchAllArticles} = articleSlice.actions
export default articleSlice.reducer