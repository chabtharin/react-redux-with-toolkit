import { configureStore } from "@reduxjs/toolkit";
import logger from "redux-logger";
import thunk from "redux-thunk";
import articleReducer from "../reducers/articleReducer";
import counterReducer from "../reducers/counterReducer";

export default configureStore({
    reducer: {
        counter: counterReducer,
        articles: articleReducer
    },
    middleware: [thunk, logger]
})