import { api } from "../utils/api";

export const fetch_all_articles = async () => {
    try {
      const result = await api.get("articles");
      console.log("fetch_all_articles:", result.data.payload);
      return result.data.payload;
    } catch (error) {
      console.log("fetch_all_articles error:", error);
    }
  };